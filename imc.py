from colorama import Fore, Back, init

LARGEUR_BANNIERE = 60
ETOILES_BANNIERE = '*' * LARGEUR_BANNIERE

# Affichage de la bannière titre
print()
print(ETOILES_BANNIERE)
print(f'{"*":<{LARGEUR_BANNIERE -2}} *')
print(f'* {"Calcul ton indice de masse corporelle (IMC).":^{LARGEUR_BANNIERE -4}} *')
print(f'{"*":<{LARGEUR_BANNIERE -2}} *')
print(ETOILES_BANNIERE)

# Demande d'infos à l'utilisateur
prenom = input('\nQuel est ton prénom : ').capitalize()
taille = int(input(f'Bonjour {prenom}, combien mesure tu (cm) ? '))
poids = int(input('Combien pèse tu (kg) ? '))

# Calcul de l'imc
imc = poids / (taille/100) ** 2

# Affichage des infos utilisateur
titre_tableau = 'Résultat'
TIRETS_TABLEAU_RESULTAT = '_' * (len(titre_tableau) + 52)
titre_tableau_taille = 'Taille'
titre_tableau_poids = 'Poids'
resultat_taille = str(taille) + ' ' + 'cm'
resultat_poids = str(poids) + ' ' + 'Kg'

print(f'\n{"Résultat":^60}')
print(f'{TIRETS_TABLEAU_RESULTAT :<60}')
print(f'{titre_tableau_taille:^30}|{titre_tableau_poids:^30}')
print(f'{resultat_taille:^30}|{resultat_poids:^30}')
print(TIRETS_TABLEAU_RESULTAT )
print('L\'indice optimal doit être entre 18.5 et 24.9')
print(f'{prenom}, ton IMC est de {imc:.2f}')

# Reset du format couleur de colorama
init(autoreset=True)

# Affichage des alertes
if imc < 18.5:
    print(Back.RED + f'{"! ATTENTION tu es TROP MAIGRE !":^60}')
elif 18.5 < imc < 24.9:
    print(Fore.GREEN + 'Corpulence normale')
elif 24.9 <= imc < 29.9:
    print(Fore.YELLOW + 'ATTENTION tu es en SURPOIDS !')
elif 29.9 <= imc < 34.9:
    print(Fore.RED + 'ATTENTION tu es en OBESITE MODEREE !!')
elif 34.9 <= imc < 39.9:
    print(Fore.RED + 'ATTENTION tu es en OBESITE SEVERE !!!')
else:
    print(Back.RED + f'{"!!!! DANGER !!!!":^60}')
print(f'{TIRETS_TABLEAU_RESULTAT :<40}')
